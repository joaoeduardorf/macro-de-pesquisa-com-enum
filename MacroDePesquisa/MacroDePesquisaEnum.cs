﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MacroDePesquisa
{
    public enum MacroDePesquisaEnum : int
    {
        [DescriptionAttribute("Personalizada")]
        PERSONALIZADA = 0,
        [DescriptionAttribute("Últimas 24 horas"), DateTimeAttribute(-1)]
        ULTIMAS24HORAS = 1,
        [DescriptionAttribute("Últimos 7 dias"), DateTimeAttribute(-7)]
        ULTIMOS7DIAS = 2,
        [DescriptionAttribute("Últimos 30 dias"), DateTimeAttribute(-30)]
        ULTIMOS30DIAS = 3,
        [DescriptionAttribute("Últimos 365 dias"), DateTimeAttribute(-365)]
        ULTIMOS365DIAS = 4,
        [DescriptionAttribute("Ontem"), Yesterday()]
        ONTEM = 5,
        [DescriptionAttribute("Semana passada"), LastWeek()]
        SEMANAPASSADA = 6,
        [DescriptionAttribute("Mês passado"), LastMonth()]
        MESPASSADO = 7,
        [DescriptionAttribute("Ano passado"), LastYear()]
        ANOPASSADO = 8,
        [DescriptionAttribute("Tudo"), All()]
        TUDO = 9
    }

    static class ExtensionMethods
    {

        public static string ToDescription(this Enum en) //ext method
        {

            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {

                object[] attrs = memInfo[0].GetCustomAttributes(
                typeof(DescriptionAttribute),

                false);

                if (attrs != null && attrs.Length > 0)

                    return ((DescriptionAttribute)attrs[0]).Description;

            }

            return en.ToString();

        }


        public static DateTime ToStartDateTime(this Enum en) //ext method
        {

            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {

                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DateTimeAttributeAbstract), false);

                if (attrs != null && attrs.Length > 0)

                    return ((DateTimeAttributeAbstract)attrs[0]).StartDateTime;

            }

            return DateTime.Now;

        }

        public static DateTime ToEndDateTime(this Enum en) //ext method
        {

            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {

                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DateTimeAttributeAbstract), false);

                if (attrs != null && attrs.Length > 0)

                    return ((DateTimeAttributeAbstract)attrs[0]).EndDateTime;

            }

            return DateTime.Now;

        }
    }
    public abstract class DateTimeAttributeAbstract : Attribute
    {
        public readonly DateTime dateTimeNow;
        public DateTime StartDateTime;
        public DateTime EndDateTime;
        public DateTimeAttributeAbstract()
        {
            dateTimeNow = DateTime.Now;
        }
        public DateTimeAttributeAbstract(int quantidadeDeDias)
        {
            dateTimeNow = DateTime.Now;
        }
    }

    public class DateTimeAttribute : DateTimeAttributeAbstract
    {
        public DateTimeAttribute(int quantidadeDeDias)
            : base(quantidadeDeDias)
        {
            this.StartDateTime = this.dateTimeNow.AddDays(quantidadeDeDias);
            this.EndDateTime = this.dateTimeNow;
        }
    }

    public class Yesterday : DateTimeAttributeAbstract
    {
        public Yesterday()
            : base()
        {
            this.StartDateTime = this.dateTimeNow.AddDays(-1).Date;
            this.EndDateTime = this.dateTimeNow.Date.AddTicks(-1);
        }
    }

    public class LastWeek : DateTimeAttributeAbstract
    {
        public LastWeek()
            : base()
        {
            this.StartDateTime = this.dateTimeNow.AddDays(-((int)this.dateTimeNow.DayOfWeek + 7)).Date;
            this.EndDateTime = this.dateTimeNow.AddDays(-((int)this.dateTimeNow.DayOfWeek)).Date.AddTicks(-1);
        }
    }

    public class LastMonth : DateTimeAttributeAbstract
    {
        public LastMonth()
            : base()
        {
            var result = this.dateTimeNow.AddDays(-((int)this.dateTimeNow.Day));
            this.StartDateTime = new DateTime(result.Year, result.Month, 01, 0, 0, 0);
            this.EndDateTime = result.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
        }
    }

    public class LastYear : DateTimeAttributeAbstract
    {
        public LastYear()
            : base()
        {
            var result = this.dateTimeNow.AddMonths(-((int)this.dateTimeNow.Month));
            this.StartDateTime = new DateTime(result.Year, 01, 01, 0, 0, 0);
            this.EndDateTime = new DateTime(result.Year, 12, 31, 23, 59, 59);
        }
    }

    public class All : DateTimeAttributeAbstract
    {
        public All()
            : base()
        {
            this.StartDateTime = DateTime.MinValue;
            this.EndDateTime = DateTime.MaxValue;
        }
    }
}
